import React from "react";
import ReactDOM from "react-dom";
import {Router, Route, IndexRoute, hashHistory} from "react-router";

import Current from "./pages/Current";
import AllEvents from "./pages/AllEvents";
import Layout from "./pages/Layout";
import Planned from "./pages/Planned";
import Completed from "./pages/Completed";

import * as ViewPageAction from "./actions/ViewPageAction";

const app = document.getElementById('app');

function changeCurrentPage(pageId) {
    return function () {
        ViewPageAction.setCurrentPage(pageId);
    }
}

ReactDOM.render(
    <Router history={hashHistory}>
        <Route path="/" component={Layout}>
            <IndexRoute component={AllEvents} onEnter={changeCurrentPage(0)}></IndexRoute>
            <Route path="current" component={Current} onEnter={changeCurrentPage(1)}></Route>
            <Route path="planned" component={Planned} onEnter={changeCurrentPage(2)}></Route>
            <Route path="completed" component={Completed} onEnter={changeCurrentPage(3)}></Route>
        </Route>
    </Router>,
    app);
