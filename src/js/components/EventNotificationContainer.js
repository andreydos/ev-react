import React from "react";
import EventNotification from "../components/EventNotification";

import EventNotificationStore from "../stores/EventNotificationStore";


export default class EventNotificationContainer extends React.Component {

    constructor() {
        super();
        this.state = {
            notifies: EventNotificationStore.getNotification()
        };
        this.updateState = this.updateState.bind(this);
    }

    componentWillMount() {
        EventNotificationStore.on("change", this.updateState);
    }

    componentWillUnmount() {
        EventNotificationStore.on("change", this.updateState);
    }

    updateState() {
        this.setState({
            notifies: EventNotificationStore.getNotification()
        });
    }

    render() {
        const {notifies} = this.state;


        const EventNotifications = notifies.map((notify) => {
            let status = notify.eventStatus === 'началось' ? 'started' : 'finished';
            return <EventNotification key={`${notify.id}-${status}-${Math.random()}`} {...notify}/>;
        });

        return (
            <div class="event-notify-wrapper">
                <ul>{EventNotifications}</ul>
            </div>
        );
    }
}
