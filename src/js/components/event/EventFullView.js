import React from "react";
import UserRoundedList from "./UserRoundedList";
import * as helpers from "../../utils/helpers";


export default class EventFullView extends React.Component {
    constructor(props) {
        super();
    }

    render() {
        const {id, name, dateStart, dateEnd, owner, location, users} = this.props;

        const monthNames = ["января", "февраля", "марта", "апреля", "мая", "июня",
            "июля", "августа", "сентября", "октября", "ноября", "декабря"
        ];

        let eventTimeState;
        let eventDateStart = new Date(dateStart);
        let eventDateEnd = new Date(dateEnd);
        let ownerFullName = owner.firstName + ' ' + owner.lastName;

        let eventDurationSeconds = (eventDateEnd - eventDateStart) / 1000;
        let minutes = Math.floor(eventDurationSeconds / 60);
        let seconds = eventDurationSeconds - minutes * 60;
        var durationTimeStr = `${helpers.strPadLeft(minutes, '', 2)} мин ${helpers.strPadLeft(seconds, '0', 2)} сек`;

        let now = new Date();

        if (eventDateStart < now && now < eventDateEnd) {
            eventTimeState = 'событие началось';
        } else if (eventDateEnd < now) {
            eventTimeState = 'событие завершилось';
        } else if (eventDateStart > now) {
            eventTimeState = 'событие планируется';
        }

        return (
            <div class="event__item" id={`event-id-${id}`}>
                <div class="event__item--date">
                    <div class="event__item--day">{eventDateStart.getDate()}</div>
                    <div class="event__item--month">{monthNames[eventDateStart.getMonth()]}</div>
                </div>
                <div class="event__item--info">
                    <div href="#" class="event__item--title">{name}</div>
                    <p class="event__item--contacts">
                        <span>организатор:</span><span class="black-text">{ownerFullName} </span>
                        <span>| {eventTimeState}</span>
                    </p>
                    <p>
                        <span>продолжительность </span><span class="black-text">{durationTimeStr}</span>
                    </p>
                    <p class="event__item--address">
                        <span>место</span><span class="black-text">{location}</span>
                    </p>
                    <p class="users-of-event">
                        <span>участники</span>
                    </p>
                    <UserRoundedList users={users}/>
                </div>
            </div>
        );
    }
}
