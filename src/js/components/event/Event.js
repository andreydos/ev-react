import React from "react";
import UserRoundedList from "./UserRoundedList";
import * as helpers from "../../utils/helpers";

function EventList(props) {
    return (
        <div class="event__item" id={`event-id-${props.id}`}>
            <div class="event__item--date">
                <div class="event__item--day">{props.eventDateStart.getDate()}</div>
                <div class="event__item--month">{props.monthNames[props.eventDateStart.getMonth()]}</div>
                <div href="#" class="event__item--title">{props.name}</div>
                <span class="event-state"> {props.eventTimeState}</span>
            </div>
            <div class='additional-info'>
                <p>
                    <span>продолжительность </span><span class="black-text">{props.durationTimeStr}</span>
                </p>
                <p class="event__item--address">
                    <span>место</span><span class="black-text">{props.location}</span>
                </p>
                <p class="users-of-event">
                    <span>участники</span>
                </p>
                <UserRoundedList users={props.users}/>
            </div>
        </div>
    );
}

function EventCard(props) {
    return (
        <div class="event__item" id={`event-id-${props.id}`}>
            <div class="event__item--date">
                <div class="event__item--day">{props.eventDateStart.getDate()}</div>
                <div class="event__item--month">{props.monthNames[props.eventDateStart.getMonth()]}</div>
            </div>
            <div class="event__item--info">
                <div href="#" class="event__item--title">{props.name}</div>
                <p class="event__item--contacts">
                    <span>организатор:</span><span class="black-text">{props.ownerFullName} </span>
                    <span>| {props.eventTimeState}</span>
                </p>
                <p>
                    <span>продолжительность </span><span class="black-text">{props.durationTimeStr}</span>
                </p>
                <p class="event__item--address">
                    <span>место</span><span class="black-text">{props.location}</span>
                </p>
                <p class="users-of-event">
                    <span>участники</span>
                </p>
                <UserRoundedList users={props.users}/>
            </div>
        </div>
    );
}

export default class EventListView extends React.Component {
    constructor(props) {
        super();
    }

    render() {
        const {id, name, dateStart, dateEnd, owner, location, users, viewType} = this.props;

        const monthNamesShort = ["янв", "февр", "марта", "апр", "мая", "июня",
            "июля", "августа", "сент", "окт", "ноября", "дек"
        ];

        const monthNamesFull = ["января", "февраля", "марта", "апреля", "мая", "июня",
            "июля", "августа", "сентября", "октября", "ноября", "декабря"
        ];

        let eventDateStart = new Date(dateStart);
        let eventDateEnd = new Date(dateEnd);
        let ownerFullName = owner.firstName + ' ' + owner.lastName;
        let eventDurationSeconds = (eventDateEnd - eventDateStart) / 1000;
        let minutes = Math.floor(eventDurationSeconds / 60);
        let seconds = eventDurationSeconds - minutes * 60;
        var durationTimeStr = `${helpers.strPadLeft(minutes, '', 2)} мин ${helpers.strPadLeft(seconds, '0', 2)} сек`;

        let now = new Date();

        let eventTimeState;
        if (eventDateStart < now && now < eventDateEnd) {
            eventTimeState = 'событие началось';
        } else if (eventDateEnd < now) {
            eventTimeState = 'событие завершилось';
        } else if (eventDateStart > now) {
            eventTimeState = 'событие планируется';
        }

        if (viewType === 1) {
            return (
                <EventList id={id}
                           eventDateStart = {eventDateStart}
                           monthNames = {monthNamesShort}
                           name = {name}
                           eventTimeState = {eventTimeState}
                           durationTimeStr = {durationTimeStr}
                           location = {location}
                           users = {users} />
            )
        } else {
            return (
                <EventCard id={id}
                           eventDateStart = {eventDateStart}
                           monthNames = {monthNamesFull}
                           name = {name}
                           ownerFullName = {ownerFullName}
                           eventTimeState = {eventTimeState}
                           durationTimeStr = {durationTimeStr}
                           location = {location}
                           users = {users} />
            )
        }
    }
}



