import React from "react";

export default class UserListItem extends React.Component {
    constructor(props) {
        super();
    }

    render() {
        const {firstName, lastName} = this.props;
        let hash = Math.random();

        return (
            <li>
                <img src={`http://pipsum.com/30x30.jpg&hash=${hash}`}/>
                <span>{firstName + ' ' + lastName}</span>
            </li>
        );
    }
}
