import React from "react";

import UserListItem from "./UserListItem";


export default class UserRoundedList extends React.Component {
    constructor(props) {
        super();
    }

    render() {
        const {users} = this.props;

        const UsersList = users.map((user) => {
            return <UserListItem key={user.id} {...user}/>;
        });

        return (
            <ul class="user-list">
                {UsersList}
            </ul>
        );
    }
}
