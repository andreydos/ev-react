import React from "react";
import EventNotificationStore from "../stores/EventNotificationStore";
import {hashHistory} from 'react-router'


export default class EventListView extends React.Component {
    constructor(props) {
        super();
        this.removeNotification = this.removeNotification.bind(this);
        this.changePath = this.changePath.bind(this);
    }

    removeNotification(e) {
        let parent = e.target.parentElement;
        if (parent.classList.contains('is-paused')) {
            parent.classList.remove('is-paused');
        }
        setTimeout(function () {
            parent.style.display = 'none';
        }, 500);
    };


    changePath(page, id) {
        return function () {
            hashHistory.replace({
                pathname: page,
                query: {eventId: 'event-id-' + id}
            })
        }.bind(this);
    }

    render() {
        const {id, eventName, eventStatus} = this.props;

        let page = eventStatus === 'началось' ? 'current' : 'completed';

        return (
            <li >
                <div class="notify-container js-fade is-paused fade-out" data-id={`${id}`}
                     onMouseEnter={EventNotificationStore.onMouseEnterHandler.bind(EventNotificationStore)}
                     onMouseLeave={EventNotificationStore.onMouseLeaveHandler.bind(EventNotificationStore)}>
                    <p class="notify-close-button" onClick={this.removeNotification}></p>
                    <p class="notify-title">Событие</p>
                    <p class="notify-event-name" onClick={this.changePath(page, id)}>{eventName}</p>
                    <p class="notify-event-status">{eventStatus}</p>
                </div>
            </li>
        );
    }
}



