import React from "react";
import {IndexLink, Link} from "react-router";

export default class Nav extends React.Component {
    constructor() {
        super();
    }

    render() {
        const {location} = this.props;
        const allEventsClass = location.pathname === "/" ? "selected" : "";
        const currentClass = location.pathname.match(/^\/current/) ? "selected" : "";
        const plannedClass = location.pathname.match(/^\/planned/) ? "selected" : "";
        const completedClass = location.pathname.match(/^\/completed/) ? "selected" : "";

        return (
            <div class="tabs">
                <div class="container">
                    <ul class="tabs__list list-clear">
                        <li class={allEventsClass}>
                            <IndexLink to="/" class="tabs__list--link" href="#">Все события</IndexLink>
                        </li>
                        <li class={currentClass}>
                            <Link to="current" class="tabs__list--link" href="#">Проходят</Link>
                        </li>
                        <li class={plannedClass}>
                            <Link to="planned" class="tabs__list--link" href="#">Планируемые</Link>
                        </li>
                        <li class={completedClass}>
                            <Link to="completed" class="tabs__list--link" href="#">Прошедшие</Link>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}
