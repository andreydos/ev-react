import React from "react";
import ViewPageStore from "../../stores/ViewPageStore";
import * as ViewPageAction from "../../actions/ViewPageAction";


export default class ViewSwitcher extends React.Component {

    constructor() {
        super();
        this.state = {
            viewType: ViewPageStore.getViewType()
        };
        this.setViewId = this.setViewId.bind(this);
        this.updateViewType = this.updateViewType.bind(this);
    }

    setViewId(viewId) {
        return function () {
            ViewPageAction.setCurrentView(viewId);
        }
    }


    updateViewType() {
        this.setState({
            viewType: ViewPageStore.getViewType()
        });
    }

    componentWillMount() {
        ViewPageStore.on("change", this.updateViewType);
    }

    componentWillUnmount() {
        ViewPageStore.removeListener("change", this.updateViewType);
    }

    render() {
        return (
            <div class="view-switcher">
                <div class={`menu-list-view ${ this.state.viewType === 1 ? 'active' : ''}`}
                     onClick={this.setViewId(1)}></div>
                <div class={`menu-expand-view ${ this.state.viewType === 0 ? 'active' : ''}`}
                     onClick={this.setViewId(0)}></div>
            </div>
        );
    }
}
