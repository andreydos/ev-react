import React from "react";


export default class Header extends React.Component {
    render() {

        return (
            <header class="header">
                <div class="top-header">
                    <a href="javascript:void(0);" class="top-header__title">Список событий</a>
                    <div class="top-header__exit"><a href="javascript:void(0);">Выход</a></div>
                </div>
            </header>
        );
    }
}
