export function handleClickOnEvents(e) {
    if (document.querySelector('.view-type-1')) {
        let eventMainNode = getNodeIfHasParentWithClass('event__item', e.target);
        if (eventMainNode) {
            let currentExpandEvent = document.querySelector('.additional-info.show');
            if (currentExpandEvent) {
                currentExpandEvent.classList.remove('show');
            }
            eventMainNode.querySelector('.additional-info').classList.add('show');
        }
    }
}

export function animatedHideNode(id) {
    let node = document.querySelector(`.notify-container[data-id="${id}"]`);
    if (node.classList.contains('is-paused')) {
        node.classList.remove('is-paused');
    }
}

export function getNodeIfHasParentWithClass(className, child) {
    if (child.classList.contains(className)) return child;

    let node = child.parentNode;
    while (node != null) {
        if (node.classList && node.classList.contains(className)) {
            return node;
        }
        node = node.parentNode;
    }
    return false;
}

export function scrollToEvent(id) {
    let node = document.getElementById(id);
    if (node) {
        node.scrollIntoView({block: "start", behavior: "smooth"});
        let scrolledY = window.scrollY;

        if (scrolledY) {
            window.scroll(0, scrolledY - 100);
        }
    }
}
export function strPadLeft(string, pad, length) {
    return (new Array(length + 1).join(pad) + string).slice(-length);
}

Array.prototype.inArray = function (comparer) {
    for (var i = 0; i < this.length; i++) {
        if (comparer(this[i])) return true;
    }
    return false;
};

Array.prototype.pushIfNotExist = function (element, comparer) {
    if (!this.inArray(comparer)) {
        this.push(element);
    }
};