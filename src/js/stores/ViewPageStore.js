import {EventEmitter} from "events";
import dispatcher from "../dispatcher";

class ViewPageStore extends EventEmitter {
    constructor() {
        super();
        this.views =
            [
                {page: 0, view: 0},
                {page: 1, view: 1},
                {page: 2, view: 0},
                {page: 3, view: 0}
            ];
        this.currentPage = 0;
        this.setViewType = this.setViewType.bind(this);
        this.syncViews = this.syncViews.bind(this);
        this.getViewType = this.getViewType.bind(this);
    }

    getViewType() {
        for (let index = 0, len = this.views.length; index < len; index++) {
            let current = this.views[index];
            if (current.page === this.currentPage) {
                return current.view;
            }
        }
    }

    setViewType(viewId) {
        this.views.forEach((current) => {
            if (current.page === this.currentPage) {
                current.view = viewId;
                return;
            }
        });
    }

    syncViews() {
        console.log('SEND VIEW to SERVER: ', {
            page: this.currentPage,
            view: this.getViewType()
        });
    }


    handleActions(action) {
        switch (action.type) {
            case "CHANGE_CURRENT_PAGE": {
                this.currentPage = action.page;
                this.emit("change");
                break;
            }
            case "CHANGE_VIEW": {
                this.setViewType(action.view);
                this.syncViews();
                this.emit("change");
                break;
            }
            case "RECEIVE_VIEWS": {
                this.views = action.views;
                this.emit("change");
                break;
            }
        }
    }

}

const viewPageStore = new ViewPageStore;
dispatcher.register(viewPageStore.handleActions.bind(viewPageStore));

export default viewPageStore;
