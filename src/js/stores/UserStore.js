import {EventEmitter} from "events";
import dispatcher from "../dispatcher";

class UserStore extends EventEmitter {
    constructor() {
        super();
        this.users =
            [
                {
                    id: 111,
                    firstName: 'Stan',
                    lastName: 'Lee'
                },
                {
                    id: 112,
                    firstName: 'Thomas',
                    lastName: 'Jeferson'
                },
                {
                    id: 113,
                    firstName: 'Gran',
                    lastName: 'Toffin'
                }
            ];
    }

    getUserById(id) {
        for (let i = 0, len = this.users.length; i < len; i++) {
            if (this.users[i].id === id)  return this.users[i];
        }
    }


    handleActions(action) {
    }

}

const userStore = new UserStore;
dispatcher.register(userStore.handleActions.bind(userStore));

export default userStore;
