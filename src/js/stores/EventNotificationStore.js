import {EventEmitter} from "events";
import dispatcher from "../dispatcher";

import * as helpers from "../utils/helpers";

class EventNotificationStore extends EventEmitter {
    constructor() {
        super();
        this.notifications = [];
        this.onMouseEnterHandler = this.onMouseEnterHandler.bind(this);
        this.onMouseLeaveHandler = this.onMouseLeaveHandler.bind(this);
        this.runRemoveNotificationsLoop = this.runRemoveNotificationsLoop.bind(this);
        this.hideAndRemoveNotification = this.hideAndRemoveNotification.bind(this);
        this.interval;
    }

    getNotification() {
        return this.notifications;
    }

    runRemoveNotificationsLoop() {
        this.interval = setInterval(() => {
            let now = new Date().getTime();
            this.notifications.forEach((notification) => {
                if (notification.closeTime < now) {
                    dispatcher.dispatch({
                        type: "REMOVE_NOTIFICATION",
                        notification: {id: notification.id, eventStatus: notification.eventStatus}
                    });
                }
            });
        }, 1000)
    }

    removeInterval() {
        clearInterval(this.interval);
    }

    onMouseEnterHandler(e) {
        let eventNotificationId = e.target.dataset.id;

        if (eventNotificationId) {
            this.notifications.map((notification) => {
                if (notification.id.toString() === eventNotificationId) {
                    notification.closeTime = notification.closeTime + 100000;
                }
                return notification;
            });
        }
    }

    onMouseLeaveHandler(e) {
        let eventNotificationId = e.target.dataset.id;

        if (eventNotificationId) {
            this.notifications.map((notification) => {
                if (notification.id.toString() === eventNotificationId) {
                    notification.closeTime = new Date().getTime() + 3000;
                }
                return notification;
            });
        }
    }

    hideAndRemoveNotification(action) {
        this.notifications = this.notifications.filter(function (notification) {
            return notification.id !== action.notification.id && notification.eventStatus == action.notification.eventStatus;
        });
        helpers.animatedHideNode(action.notification.id);
    }

    handleActions(action) {
        switch (action.type) {
            case "ADD_NOTIFICATION": {
                this.notifications.push(action.notification);
                this.emit("change");
                break;
            }
            case "REMOVE_NOTIFICATION": {
                this.hideAndRemoveNotification(action);
                setTimeout( () => {
                    this.emit("change");
                }, 500);
                break;
            }
        }
    }

}

const eventNotificationStore = new EventNotificationStore;
dispatcher.register(eventNotificationStore.handleActions.bind(eventNotificationStore));

export default eventNotificationStore;
