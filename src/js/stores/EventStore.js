import {EventEmitter} from "events";

import dispatcher from "../dispatcher";

import * as EventService from "../services/EventService";

import * as EventNotificationAction from "../actions/EventNotificationAction";

class EventStore extends EventEmitter {
    constructor() {
        super();
        this.events = [];
        this.isLoading = false;
        this.interval;
        this.loopOverEvents = this.loopOverEvents.bind(this);
    }

    loopOverEvents() {
        this.interval = setInterval( () => {
            let now = new Date().toString();
            this.events.forEach( (event) => {
                if (new Date(event.dateStart).toString() === now || new Date(event.dateEnd).toString() === now) {
                    this.emit('change')
                }
                if (new Date(event.dateStart).toString() === now) {
                    EventNotificationAction.addNotification({
                        id: event.id,
                        eventName: event.name,
                        eventStatus: 'началось'
                    });
                }
                if (new Date(event.dateEnd).toString() === now) {
                    EventNotificationAction.addNotification({
                        id: event.id,
                        eventName: event.name,
                        eventStatus: 'закончилось'
                    });
                }
            });
        }, 1000);
    }

    getAll() {
        return this.events;
    }

    getPlanned() {
        return this.events.filter(function (event) {
            return new Date(event.dateStart) > new Date();
        });
    }

    getCurrent() {
        return this.events.filter(function (event) {
            let now = new Date();
            return new Date(event.dateStart) < now && now < new Date(event.dateEnd);
        });
    }

    getCompleted() {
        return this.events.filter(function (event) {
            return new Date(event.dateEnd) < new Date();
        });

    }

    receiveEventsHandler(action) {
        this.isLoading = false;
        this.events = action.events;
        this.events.sort(function (a, b) {
            return new Date(b.dateStart) - new Date(a.dateStart);
        });

        clearInterval(this.interval);
        this.loopOverEvents();
    }

    addNewEventsHandler(action) {
        this.isLoading = false;
        this.events.push(...action.events);
        this.events.sort(function (a, b) {
            return new Date(b.dateStart) - new Date(a.dateStart);
        });
    }

    updateUserHandler(action) {
        let updates = action.eventsUpdate.events;

        updates.forEach( (update) => {
            this.events.forEach( (event) => {
                if (update.event === event.id) {
                    EventService.bindEvents(update, event);
                    EventService.unbindEvents(update, event);
                }
            })
        });
    }

    handleActions(action) {
        switch (action.type) {
            case "FETCH_EVENTS": {
                this.isLoading = true;
                this.emit("change");
                break;
            }
            case "RECEIVE_EVENTS": {
                this.receiveEventsHandler(action);
                this.emit("change");
                break;
            }
            case "ADD_NEW_EVENTS": {
                this.addNewEventsHandler(action);
                this.emit("change");
                break;
            }
            case "UPDATE_USERS": {
                this.updateUserHandler(action);
                this.emit("change");
                break;
            }
        }
    }

}

const eventStore = new EventStore;
dispatcher.register(eventStore.handleActions.bind(eventStore));

export default eventStore;
