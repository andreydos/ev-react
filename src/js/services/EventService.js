import UserStore from "../stores/UserStore";


export function bindEvents(updateItem, event) {
    updateItem.bind && updateItem.bind.forEach(function (newUserId) {
        const user = UserStore.getUserById(newUserId);

        event.users.pushIfNotExist(user, function (e) {
            return e.id === user.id;
        });
    });
}

export function unbindEvents(updateItem, event) {
    updateItem.unbind && updateItem.unbind.forEach(function (deleteUserId) {
        for (var i = 0; i < event.users.length; i++) {
            let currentUser = event.users[i];
            if (currentUser.id === deleteUserId) {
                event.users.splice(i, 1);
            }
        }
    });
}