import React from "react";

import Event from "../components/event/Event";
import Spinner from "../components/Spinner";

import EventStore from "../stores/EventStore";
import ViewPageStore from "../stores/ViewPageStore";
import EventNotificationStore from "../stores/EventNotificationStore";

import * as helpers from "../utils/helpers";

export default class Current extends React.Component {
    constructor() {
        super();
        this.getEvents = this.getEvents.bind(this);
        this.onMouseEnterHandler = this.onMouseEnterHandler.bind(this);
        this.onMouseLeaveHandler = this.onMouseLeaveHandler.bind(this);
        this.state = {
            events: EventStore.getCurrent(),
            view: ViewPageStore.getViewType()
        };
        this.hover = false;
    }

    componentWillMount() {
        EventStore.on("change", this.getEvents);
        ViewPageStore.on("change", this.getEvents);
        EventNotificationStore.on("change", this.getEvents);
    }

    componentDidMount() {
        document.querySelector('.event-container').addEventListener('click', helpers.handleClickOnEvents);
    }

    shouldComponentUpdate() {
        return !this.hover;
    }

    componentDidUpdate() {
        helpers.scrollToEvent(this.props.location.query.eventId);
    }

    componentWillUnmount() {
        EventStore.removeListener("change", this.getEvents);
        ViewPageStore.removeListener("change", this.getEvents);
        EventNotificationStore.removeListener("change", this.getEvents);
        document.querySelector('.event-container').removeEventListener('click', helpers.handleClickOnEvents);
    }

    getEvents() {
        this.setState({
            events: EventStore.getCurrent(),
            view: ViewPageStore.getViewType()
        });
    }

    onMouseEnterHandler() {
        this.hover = true;
    }

    onMouseLeaveHandler() {
        this.hover = false;
    }


    render() {
        if (EventStore.isLoading) {

            return (<div>
                <Spinner />
            </div>)

        } else {

            const {events} = this.state;

            const EventComponents = events.map((event) => {
                event.viewType = this.state.view;
                return <Event key={event.id} {...event}/>;
            });

            return (
                <div class={`event-wrapper view-type-${this.state.view}`} onMouseEnter={this.onMouseEnterHandler}
                     onMouseLeave={this.onMouseLeaveHandler}>
                    <ul>{EventComponents}</ul>
                </div>
            );
        }
    }
}
