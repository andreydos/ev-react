import React from "react";

import Header from "../components/layout/Header";
import Nav from "../components/layout/Nav";
import ViewSwitcher from "../components/layout/ViewSwitcher";
import EventNotificationStore from "../stores/EventNotificationStore";
import EventNotificationContainer from "../components/EventNotificationContainer";

import * as EventActions from "../actions/EventActions";

export default class Layout extends React.Component {

    constructor() {
        super();
    }

    componentDidMount() {
        EventActions.loadEvents();
        EventActions.startFakeThings();
        EventNotificationStore.runRemoveNotificationsLoop();
    }

    componentWillUnmount() {
        EventNotificationStore.removeInterval();
    }

    render() {
        const {location} = this.props;

        return (
            <div>
                <Header/>
                <Nav location={location}/>
                <div class="container event-container">
                    <ViewSwitcher />
                    {this.props.children}
                </div>
                < EventNotificationContainer />
            </div>
        );
    }
}
