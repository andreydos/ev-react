import React from "react";

import Event from "../components/event/Event";
import Spinner from "../components/Spinner";

import EventStore from "../stores/EventStore";
import ViewPageStore from "../stores/ViewPageStore";

import * as helpers from "../utils/helpers";


export default class AllEvents extends React.Component {
    constructor() {
        super();
        this.getEvents = this.getEvents.bind(this);
        this.state = {
            events: EventStore.getAll(),
            view: ViewPageStore.getViewType()
        };
    }

    componentWillMount() {
        EventStore.on("change", this.getEvents);
        ViewPageStore.on("change", this.getEvents);
    }

    componentDidMount() {
        document.querySelector('.event-container').addEventListener('click', helpers.handleClickOnEvents);
    }

    componentWillUnmount() {
        EventStore.removeListener("change", this.getEvents);
        ViewPageStore.removeListener("change", this.getEvents);
        document.querySelector('.event-container').removeEventListener('click', helpers.handleClickOnEvents);
    }

    getEvents() {

        this.setState({
            events: EventStore.getAll(),
            view: ViewPageStore.getViewType()
        });
    }

    render() {
        if (EventStore.isLoading) {

            return <div>
                <Spinner />
            </div>

        } else {

            const {events} = this.state;

            const EventComponents = events.map((event) => {
                event.viewType = this.state.view;
                    return <Event key={event.id} {...event}/>;
            });

            return (
                <div class={`event-wrapper view-type-${this.state.view}`}>
                    <ul>{EventComponents}</ul>
                </div>
            );
        }
    }
}
