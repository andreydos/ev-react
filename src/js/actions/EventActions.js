import dispatcher from "../dispatcher";


export function loadEvents() {
    dispatcher.dispatch({type: "FETCH_EVENTS"});
}

export function startFakeThings() {
    setTimeout(function () {
        let now = new Date().getTime();
        let dateStart1 = new Date((now + 3000 )).toUTCString();
        let dateEnd1 = new Date((now + 12000 )).toUTCString();
        let dateStart2 = new Date((now + 5000 )).toUTCString();
        let dateEnd2 = new Date((now + 18000 )).toUTCString();
        dispatcher.dispatch({
            type: "RECEIVE_EVENTS", events: [
                {
                    id: 1,
                    name: "Football match",
                    dateStart: dateStart1,
                    dateEnd: dateEnd1,
                    location: 'Baltimor',
                    owner: {
                        id: 2,
                        firstName: 'John',
                        lastName: 'Brudniy'
                    },
                    users: [
                        {
                            id: 3,
                            firstName: 'Alex',
                            lastName: 'Levinski'
                        },
                        {
                            id: 4,
                            firstName: 'Tori',
                            lastName: 'Lee'
                        }
                    ]
                },
                {
                    id: 9,
                    name: "Baseball match",
                    dateStart: dateStart2,
                    dateEnd: dateEnd2,
                    location: 'San Paolo',
                    owner: {
                        id: 10,
                        firstName: 'John',
                        lastName: 'Brudniy'
                    },
                    users: [
                        {
                            id: 11,
                            firstName: 'Aphex',
                            lastName: 'Twix'
                        },
                        {
                            id: 12,
                            firstName: 'Jane',
                            lastName: 'Osera'
                        }
                    ]
                },

                {
                    id: 901,
                    name: "Football match",
                    dateStart: new Date((now - 18000 )).toUTCString(),
                    dateEnd: new Date((now + 1800000 )).toUTCString(),
                    location: 'Baltimor',
                    owner: {
                        id: 2,
                        firstName: 'John',
                        lastName: 'Brudniy'
                    },
                    users: [
                        {
                            id: 3,
                            firstName: 'Alex',
                            lastName: 'Levinski'
                        },
                        {
                            id: 4,
                            firstName: 'Tori',
                            lastName: 'Lee'
                        }
                    ]
                },

                {
                    id: 809,
                    name: "Football match",
                    dateStart: new Date((now - 18000 )).toUTCString(),
                    dateEnd: new Date((now + 1800000 )).toUTCString(),
                    location: 'Baltimor',
                    owner: {
                        id: 2,
                        firstName: 'John',
                        lastName: 'Brudniy'
                    },
                    users: [
                        {
                            id: 3,
                            firstName: 'Alex',
                            lastName: 'Levinski'
                        },
                        {
                            id: 4,
                            firstName: 'Tori',
                            lastName: 'Lee'
                        }
                    ]
                },

                {
                    id: 808,
                    name: "Football match",
                    dateStart: new Date((now - 18000 )).toUTCString(),
                    dateEnd: new Date((now + 1800000 )).toUTCString(),
                    location: 'Baltimor',
                    owner: {
                        id: 2,
                        firstName: 'John',
                        lastName: 'Brudniy'
                    },
                    users: [
                        {
                            id: 3,
                            firstName: 'Alex',
                            lastName: 'Levinski'
                        },
                        {
                            id: 4,
                            firstName: 'Tori',
                            lastName: 'Lee'
                        }
                    ]
                },

                {
                    id: 807,
                    name: "Football match",
                    dateStart: new Date((now - 18000 )).toUTCString(),
                    dateEnd: new Date((now + 1800000 )).toUTCString(),
                    location: 'Baltimor',
                    owner: {
                        id: 2,
                        firstName: 'John',
                        lastName: 'Brudniy'
                    },
                    users: [
                        {
                            id: 3,
                            firstName: 'Alex',
                            lastName: 'Levinski'
                        },
                        {
                            id: 4,
                            firstName: 'Tori',
                            lastName: 'Lee'
                        }
                    ]
                },

                {
                    id: 806,
                    name: "Football match",
                    dateStart: new Date((now - 18000 )).toUTCString(),
                    dateEnd: new Date((now + 1800000 )).toUTCString(),
                    location: 'Baltimor',
                    owner: {
                        id: 2,
                        firstName: 'John',
                        lastName: 'Brudniy'
                    },
                    users: [
                        {
                            id: 3,
                            firstName: 'Alex',
                            lastName: 'Levinski'
                        },
                        {
                            id: 4,
                            firstName: 'Tori',
                            lastName: 'Lee'
                        }
                    ]
                },


                {
                    id: 805,
                    name: "Football match",
                    dateStart: new Date((now - 18000 )).toUTCString(),
                    dateEnd: new Date((now + 1800000 )).toUTCString(),
                    location: 'Baltimor',
                    owner: {
                        id: 2,
                        firstName: 'John',
                        lastName: 'Brudniy'
                    },
                    users: [
                        {
                            id: 3,
                            firstName: 'Alex',
                            lastName: 'Levinski'
                        },
                        {
                            id: 4,
                            firstName: 'Tori',
                            lastName: 'Lee'
                        }
                    ]
                },


                {
                    id: 804,
                    name: "Football match",
                    dateStart: new Date((now - 18000 )).toUTCString(),
                    dateEnd: new Date((now + 1800000 )).toUTCString(),
                    location: 'Baltimor',
                    owner: {
                        id: 2,
                        firstName: 'John',
                        lastName: 'Brudniy'
                    },
                    users: [
                        {
                            id: 3,
                            firstName: 'Alex',
                            lastName: 'Levinski'
                        },
                        {
                            id: 4,
                            firstName: 'Tori',
                            lastName: 'Lee'
                        }
                    ]
                },

                {
                    id: 803,
                    name: "Football match",
                    dateStart: new Date((now - 18000 )).toUTCString(),
                    dateEnd: new Date((now + 1800000 )).toUTCString(),
                    location: 'Baltimor',
                    owner: {
                        id: 2,
                        firstName: 'John',
                        lastName: 'Brudniy'
                    },
                    users: [
                        {
                            id: 3,
                            firstName: 'Alex',
                            lastName: 'Levinski'
                        },
                        {
                            id: 4,
                            firstName: 'Tori',
                            lastName: 'Lee'
                        }
                    ]
                },

                {
                    id: 802,
                    name: "Football match",
                    dateStart: new Date((now - 18000 )).toUTCString(),
                    dateEnd: new Date((now + 1800000 )).toUTCString(),
                    location: 'Baltimor',
                    owner: {
                        id: 2,
                        firstName: 'John',
                        lastName: 'Brudniy'
                    },
                    users: [
                        {
                            id: 3,
                            firstName: 'Alex',
                            lastName: 'Levinski'
                        },
                        {
                            id: 4,
                            firstName: 'Tori',
                            lastName: 'Lee'
                        }
                    ]
                },


                {
                    id: 801,
                    name: "Football match",
                    dateStart: new Date((now - 18000 )).toUTCString(),
                    dateEnd: new Date((now + 1800000 )).toUTCString(),
                    location: 'Baltimor',
                    owner: {
                        id: 2,
                        firstName: 'John',
                        lastName: 'Brudniy'
                    },
                    users: [
                        {
                            id: 3,
                            firstName: 'Alex',
                            lastName: 'Levinski'
                        },
                        {
                            id: 4,
                            firstName: 'Tori',
                            lastName: 'Lee'
                        }
                    ]
                },

                {
                    id: 902,
                    name: "Football match",
                    dateStart: new Date((now - 18000 )).toUTCString(),
                    dateEnd: new Date((now + 1800000 )).toUTCString(),
                    location: 'Baltimor',
                    owner: {
                        id: 2,
                        firstName: 'John',
                        lastName: 'Brudniy'
                    },
                    users: [
                        {
                            id: 3,
                            firstName: 'Alex',
                            lastName: 'Levinski'
                        },
                        {
                            id: 4,
                            firstName: 'Tori',
                            lastName: 'Lee'
                        }
                    ]
                },

                {
                    id: 903,
                    name: "Football match",
                    dateStart: new Date((now - 18000 )).toUTCString(),
                    dateEnd: new Date((now + 1800000 )).toUTCString(),
                    location: 'Baltimor',
                    owner: {
                        id: 2,
                        firstName: 'John',
                        lastName: 'Brudniy'
                    },
                    users: [
                        {
                            id: 3,
                            firstName: 'Alex',
                            lastName: 'Levinski'
                        },
                        {
                            id: 4,
                            firstName: 'Tori',
                            lastName: 'Lee'
                        }
                    ]
                },

                {
                    id: 904,
                    name: "Football match",
                    dateStart: new Date((now - 18000 )).toUTCString(),
                    dateEnd: new Date((now + 1800000 )).toUTCString(),
                    location: 'Baltimor',
                    owner: {
                        id: 2,
                        firstName: 'John',
                        lastName: 'Brudniy'
                    },
                    users: [
                        {
                            id: 3,
                            firstName: 'Alex',
                            lastName: 'Levinski'
                        },
                        {
                            id: 4,
                            firstName: 'Tori',
                            lastName: 'Lee'
                        }
                    ]
                },

                {
                    id: 905,
                    name: "Football match",
                    dateStart: new Date((now - 18000 )).toUTCString(),
                    dateEnd: new Date((now + 1800000 )).toUTCString(),
                    location: 'Baltimor',
                    owner: {
                        id: 2,
                        firstName: 'John',
                        lastName: 'Brudniy'
                    },
                    users: [
                        {
                            id: 3,
                            firstName: 'Alex',
                            lastName: 'Levinski'
                        },
                        {
                            id: 4,
                            firstName: 'Tori',
                            lastName: 'Lee'
                        }
                    ]
                },

                {
                    id: 906,
                    name: "Football match",
                    dateStart: new Date((now - 18000 )).toUTCString(),
                    dateEnd: new Date((now + 1800000 )).toUTCString(),
                    location: 'Baltimor',
                    owner: {
                        id: 2,
                        firstName: 'John',
                        lastName: 'Brudniy'
                    },
                    users: [
                        {
                            id: 3,
                            firstName: 'Alex',
                            lastName: 'Levinski'
                        },
                        {
                            id: 4,
                            firstName: 'Tori',
                            lastName: 'Lee'
                        }
                    ]
                },

                {
                    id: 907,
                    name: "Football match",
                    dateStart: new Date((now - 18000 )).toUTCString(),
                    dateEnd: new Date((now + 1800000 )).toUTCString(),
                    location: 'Baltimor',
                    owner: {
                        id: 2,
                        firstName: 'John',
                        lastName: 'Brudniy'
                    },
                    users: [
                        {
                            id: 3,
                            firstName: 'Alex',
                            lastName: 'Levinski'
                        },
                        {
                            id: 4,
                            firstName: 'Tori',
                            lastName: 'Lee'
                        }
                    ]
                },

                {
                    id: 908,
                    name: "Football match",
                    dateStart: new Date((now - 18000 )).toUTCString(),
                    dateEnd: new Date((now + 1800000 )).toUTCString(),
                    location: 'Baltimor',
                    owner: {
                        id: 2,
                        firstName: 'John',
                        lastName: 'Brudniy'
                    },
                    users: [
                        {
                            id: 3,
                            firstName: 'Alex',
                            lastName: 'Levinski'
                        },
                        {
                            id: 4,
                            firstName: 'Tori',
                            lastName: 'Lee'
                        }
                    ]
                },

                {
                    id: 909,
                    name: "Football match",
                    dateStart: new Date((now - 18000 )).toUTCString(),
                    dateEnd: new Date((now + 1800000 )).toUTCString(),
                    location: 'Baltimor',
                    owner: {
                        id: 2,
                        firstName: 'John',
                        lastName: 'Brudniy'
                    },
                    users: [
                        {
                            id: 3,
                            firstName: 'Alex',
                            lastName: 'Levinski'
                        },
                        {
                            id: 4,
                            firstName: 'Tori',
                            lastName: 'Lee'
                        }
                    ]
                },

                {
                    id: 21,
                    name: "Baseball match",
                    dateStart: '2016-12-04T20:19:00.000Z',
                    dateEnd: '2016-12-04T20:21:00.000Z',
                    location: 'San Paolo',
                    owner: {
                        id: 10,
                        firstName: 'Jimmy',
                        lastName: 'McKensy'
                    },
                    users: [
                        {
                            id: 11,
                            firstName: 'Alex',
                            lastName: 'Levinski'
                        },
                        {
                            id: 12,
                            firstName: 'Jane',
                            lastName: 'Osera'
                        }
                    ]
                },

                {
                    id: 22,
                    name: "Baseball match",
                    dateStart: '2016-12-16T20:19:00.000Z',
                    dateEnd: '2016-12-16T20:21:00.000Z',
                    location: 'San Paolo',
                    owner: {
                        id: 10,
                        firstName: 'John',
                        lastName: 'Brudniy'
                    },
                    users: [
                        {
                            id: 11,
                            firstName: 'Alex',
                            lastName: 'Levinski'
                        },
                        {
                            id: 12,
                            firstName: 'Jane',
                            lastName: 'Osera'
                        }
                    ]
                },

                {
                    id: 5,
                    name: "Football super match",
                    dateStart: '2016-12-11T20:20:00.000Z',
                    dateEnd: '2016-12-11T22:20:00.000Z',
                    location: 'LA',
                    owner: {
                        id: 5,
                        firstName: 'John',
                        lastName: 'Brudniy'
                    },
                    users: [
                        {
                            id: 7,
                            firstName: 'Alex',
                            lastName: 'Levinski'
                        },
                        {
                            id: 8,
                            firstName: 'Tori',
                            lastName: 'Lee'
                        }
                    ]
                },
                {
                    id: 13,
                    name: "Swimming match",
                    dateStart: '2016-12-16T20:19:00.000Z',
                    dateEnd: '2016-12-16T20:21:00.000Z',
                    location: 'Redmont',
                    owner: {
                        id: 14,
                        firstName: 'Joan',
                        lastName: 'Dipaito'
                    },
                    users: [
                        {
                            id: 15,
                            firstName: 'Mike',
                            lastName: 'Jonson'
                        },
                        {
                            id: 16,
                            firstName: 'Tomy',
                            lastName: 'Lee'
                        }
                    ]
                },
                {
                    id: 33,
                    name: "Swimming match",
                    dateStart: '2016-12-04T20:19:00.000Z',
                    dateEnd: '2016-12-04T20:21:00.000Z',
                    location: 'Redmont',
                    owner: {
                        id: 14,
                        firstName: 'Joan',
                        lastName: 'Dipaito'
                    },
                    users: [
                        {
                            id: 15,
                            firstName: 'Mike',
                            lastName: 'Jonson'
                        },
                        {
                            id: 16,
                            firstName: 'Tomy',
                            lastName: 'Lee'
                        }
                    ]
                },
                {
                    id: 17,
                    name: "Snooker match",
                    dateStart: '2016-12-17T20:19:00.000Z',
                    dateEnd: '2016-12-17T20:21:00.000Z',
                    location: 'Boston',
                    owner: {
                        id: 18,
                        firstName: 'Geremi',
                        lastName: 'Brudniy'
                    },
                    users: [
                        {
                            id: 19,
                            firstName: 'Alex',
                            lastName: 'Levinski'
                        },
                        {
                            id: 20,
                            firstName: 'Tori',
                            lastName: 'Lee'
                        }
                    ]
                }
            ]
        });
    }, 1000);

    setTimeout(function () {
        dispatcher.dispatch({
            type: "UPDATE_USERS", eventsUpdate: {
                events: [
                    {
                        event: 1,
                        bind: [111, 112],
                        unbind: [3]
                    },
                    {
                        event: 9,
                        bind: [113],
                        unbind: [11, 12]
                    }
                ]
            }
        });
    }, 5000);

    setTimeout(function () {
        dispatcher.dispatch({
            type: "ADD_NEW_EVENTS", events: [
                {
                    id: 43,
                    name: "Party",
                    dateStart: '2016-12-15T20:09:00.000Z',
                    dateEnd: '2016-12-15T20:21:00.000Z',
                    location: 'NY',
                    owner: {
                        id: 14,
                        firstName: 'Joan',
                        lastName: 'Dipaito'
                    },
                    users: [
                        {
                            id: 15,
                            firstName: 'Mike',
                            lastName: 'Jonson'
                        },
                        {
                            id: 16,
                            firstName: 'Tomy',
                            lastName: 'Lee'
                        }
                    ]
                },
                {
                    id: 53,
                    name: "Camping",
                    dateStart: '2016-12-18T20:19:00.000Z',
                    dateEnd: '2016-12-18T20:21:00.000Z',
                    location: 'Redmont',
                    owner: {
                        id: 14,
                        firstName: 'Joan',
                        lastName: 'Dipaito'
                    },
                    users: [
                        {
                            id: 15,
                            firstName: 'Mike',
                            lastName: 'Jonson'
                        },
                        {
                            id: 16,
                            firstName: 'Tomy',
                            lastName: 'Lee'
                        }
                    ]
                }
            ]
        });
    }, 3000);

    setTimeout(function () {
        dispatcher.dispatch({
            type: "ADD_NEW_EVENTS", events: [
                {
                    id: 44,
                    name: "Party on the beach",
                    dateStart: '2016-12-15T20:16:00.000Z',
                    dateEnd: '2016-12-15T20:21:00.000Z',
                    location: 'NY',
                    owner: {
                        id: 14,
                        firstName: 'Joan',
                        lastName: 'Dipaito'
                    },
                    users: [
                        {
                            id: 15,
                            firstName: 'Mike',
                            lastName: 'Jonson'
                        },
                        {
                            id: 16,
                            firstName: 'Tomy',
                            lastName: 'Lee'
                        }
                    ]
                },
                {
                    id: 54,
                    name: "Camping at the river",
                    dateStart: '2016-12-15T10:33:45.000Z',
                    dateEnd: '2016-12-15T20:11:00.000Z',
                    location: 'Redmont',
                    owner: {
                        id: 14,
                        firstName: 'Joan',
                        lastName: 'Dipaito'
                    },
                    users: [
                        {
                            id: 15,
                            firstName: 'Mike',
                            lastName: 'Jonson'
                        },
                        {
                            id: 16,
                            firstName: 'Tomy',
                            lastName: 'Lee'
                        }
                    ]
                }
            ]
        });
    }, 3500);

}

