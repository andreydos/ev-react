import dispatcher from "../dispatcher";
// import EventStore from "../stores/EventStore";

// let interval;

export function addNotification(notification) {
    notification.closeTime = new Date().getTime() + 3000;
    dispatcher.dispatch({type: "ADD_NOTIFICATION", notification: notification});
}

// export function runNotificationChecker() {
// interval = setInterval(function () {
//     let events = EventStore.getAll();
//     for (var index = 0, len = events.length; index < len; index++) {
//         var current = events[index];
//         if (new Date(current.dateStart).toString() === new Date().toString()) {
//             console.log('ADD ID: ', current.id);
//             addNotification({
//                 id: current.id,
//                 eventName: current.name,
//                 eventStatus: 'началось'
//             });
//         }
//         if (new Date(current.dateEnd).toString() === new Date().toString()) {
//             console.log('ADD ID: ', current.id);
//
//             addNotification({
//                 id: current.id,
//                 eventName: current.name,
//                 eventStatus: 'закончилось'
//             });
//         }
//     }
// }, 1000);
// }

// export function removeInterval() {
//     clearInterval(interval);
// }