import dispatcher from "../dispatcher";

export function setCurrentPage(pageId) {
    dispatcher.dispatch({type: "CHANGE_CURRENT_PAGE", page: pageId});
}

export function setCurrentView(viewId) {
    dispatcher.dispatch({type: "CHANGE_VIEW", view: viewId});
}



